require('dotenv').config();

const app = require('./app');

const port = 8081;
app.listen(port, () => {
  console.log(`App running on port ${port}.`);
});
