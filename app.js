require('dotenv').config(); // Load environment variables from .env file

const express = require('express');
const morgan = require('morgan');
const helmet = require('helmet');
const { Pool } = require('pg');

const connectionString = process.env.DB_CONNECTION_STRING;
const pool = new Pool({ connectionString });

const app = express();
app.use(helmet());
app.use(express.json());




if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'));
}


if(pool){ // mysql is started && connected successfully.
  console.log("yess");
}else{
  console.log('Cant connect to db, Check ur db connection');
}

// Get all food
const getFood = (req, res) => {
  console.log("burda 1")
  pool.query('SELECT * FROM food', (error, results) => {
    if (error) {
      console.log(error)
      throw error;
    }
    res.status(200).json({
      status: 'sucess',
      requestTime: req.requestTime,
      data: results.rows,
    });
    console.log("burda 2")
  });
};

// Get food by id
const getFoodById = (req, res) => {
  const reqId = parseInt(req.params.id);
  pool.query('SELECT * FROM food WHERE id = $1', [reqId], (error, results) => {
    if (error) {
      throw error;
    }
    res.status(200).json({
      status: 'sucess',
      requestTime: req.requestTime,
      data: results.rows,
    });
  });
};

// Create food
const newFood = (req, res) => {
  const { dish, country } = req.body;
  pool.query(
    'INSERT INTO food (dish, country) VALUES ($1, $2)',
    [dish, country],
    (error, results) => {
      if (error) {
        throw error;
      }
      res.status(201).send(`New food added`);
    }
  );
};

// Update food
const updateFood = (req, res) => {
  const reqId = parseInt(req.params.id);
  const { dish, country } = req.body;
  pool.query(
    'UPDATE food SET dish = $1, country = $2 WHERE id = $3',
    [dish, country, reqId],
    (error, results) => {
      if (error) {
        throw error;
      }
      res.status(200).send(`Food modified with ID: ${reqId}`);
    }
  );
};

const deleteFood = (req, res) => {
  const reqId = parseInt(req.params.id);
  pool.query('DELETE FROM food WHERE id = $1', [reqId], (error, results) => {
    if (error) {
      throw error;
    }
    res.status(200).send(`Food deleted with ID: ${reqId}`);
  });
};

// Get food by id
const getsa = (req, res) => {
  console.log("düstün");
};

app.route('/food').get(getFood).post(newFood);
app.route('/food/:id').get(getFoodById).put(updateFood).delete(deleteFood);

app.route('/sa').get(getFood);
module.exports = app;
